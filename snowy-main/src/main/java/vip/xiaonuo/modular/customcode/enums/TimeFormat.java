package vip.xiaonuo.modular.customcode.enums;

import org.springframework.stereotype.Component;

public enum TimeFormat {
    //枚举常量
    YEAR(0), YEAR_MONTH(1), YEAR_MONTH_DAY(2),NOTHING(3);

    public final int timeFormatValue;

   TimeFormat(int i) {
        this.timeFormatValue = i;
    }
}
