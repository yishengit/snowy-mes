/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.warehouse.controller;

import vip.xiaonuo.core.annotion.BusinessLog;
import vip.xiaonuo.core.annotion.DataScope;
import vip.xiaonuo.core.annotion.Permission;
import vip.xiaonuo.core.enums.LogAnnotionOpTypeEnum;
import vip.xiaonuo.core.pojo.response.ResponseData;
import vip.xiaonuo.core.pojo.response.SuccessResponseData;
import vip.xiaonuo.modular.protype.param.ProTypeParam;
import vip.xiaonuo.modular.warehouse.param.WareHouseParam;
import vip.xiaonuo.modular.warehouse.service.WareHouseService;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.annotation.Resource;
import java.util.List;

/**
 * 仓库管理控制器
 *
 * @author czw
 * @date 2022-07-27 16:28:21
 */
@RestController
public class WareHouseController {

    @Resource
    private WareHouseService wareHouseService;

    /**
     * 查询仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    @Permission
    @GetMapping("/wareHouse/page")
    @BusinessLog(title = "仓库管理_查询", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData page(WareHouseParam wareHouseParam) {
        return new SuccessResponseData(wareHouseService.page(wareHouseParam));
    }

    /**
     * 添加仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    @Permission
    @PostMapping("/wareHouse/add")
    @BusinessLog(title = "仓库管理_增加", opType = LogAnnotionOpTypeEnum.ADD)
    public ResponseData add(@RequestBody @Validated(WareHouseParam.add.class) WareHouseParam wareHouseParam) {
            wareHouseService.add(wareHouseParam);
        return new SuccessResponseData();
    }

    /**
     * 删除仓库管理，可批量删除
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    @Permission
    @PostMapping("/wareHouse/delete")
    @BusinessLog(title = "仓库管理_删除", opType = LogAnnotionOpTypeEnum.DELETE)
    public ResponseData delete(@RequestBody @Validated(WareHouseParam.delete.class) List<WareHouseParam> wareHouseParamList) {
            wareHouseService.delete(wareHouseParamList);
        return new SuccessResponseData();
    }

    /**
     * 编辑仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    @Permission
    @PostMapping("/wareHouse/edit")
    @BusinessLog(title = "仓库管理_编辑", opType = LogAnnotionOpTypeEnum.EDIT)
    public ResponseData edit(@RequestBody @Validated(WareHouseParam.edit.class) WareHouseParam wareHouseParam) {
            wareHouseService.edit(wareHouseParam);
        return new SuccessResponseData();
    }

    /**
     * 查看仓库管理
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    @Permission
    @GetMapping("/wareHouse/detail")
    @BusinessLog(title = "仓库管理_查看", opType = LogAnnotionOpTypeEnum.DETAIL)
    public ResponseData detail(@Validated(WareHouseParam.detail.class) WareHouseParam wareHouseParam) {
        return new SuccessResponseData(wareHouseService.detail(wareHouseParam));
    }

    /**
     * 仓库管理列表
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    @Permission
    @GetMapping("/wareHouse/list")
    @BusinessLog(title = "仓库管理_列表", opType = LogAnnotionOpTypeEnum.QUERY)
    public ResponseData list(WareHouseParam wareHouseParam) {
        return new SuccessResponseData(wareHouseService.list(wareHouseParam));
    }

    /**
     * 导出系统用户
     *
     * @author czw
     * @date 2022-07-27 16:28:21
     */
    @Permission
    @GetMapping("/wareHouse/export")
    @BusinessLog(title = "仓库管理_导出", opType = LogAnnotionOpTypeEnum.EXPORT)
    public void export(WareHouseParam wareHouseParam) {
        wareHouseService.export(wareHouseParam);
    }

    /**
     * 获取组织机构树
     *
     * @author xuyuxiang
     * @date 2020/3/26 11:55
     */
    @Permission
    @GetMapping("/wareHouse/tree")
    @BusinessLog(title = "仓库管理树", opType = LogAnnotionOpTypeEnum.TREE)
    public ResponseData tree(WareHouseParam wareHouseParam) {
        return new SuccessResponseData(wareHouseService.tree(wareHouseParam));
    }

}
