package vip.xiaonuo.modular.workstep.result;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import vip.xiaonuo.modular.workstep.entity.WorkStep;

@Data
public class WorkStepResult extends WorkStep {
    //名称
    private String reportRightName;

}
