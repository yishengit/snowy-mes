package vip.xiaonuo.modular.workreport.enums;

import lombok.Getter;

@Getter
public enum WorkReportDistinctionEnum {
    /*
    从报工添加
    * */
    WORK_REPORT_ADD(1,"从报工添加"),
    TASK_ADD(0,"从任务添加")
    ;

    private final Integer code;

    private final String message;

    WorkReportDistinctionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
