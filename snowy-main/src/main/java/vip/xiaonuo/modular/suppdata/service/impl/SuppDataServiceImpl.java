/*
Copyright [2020] [https://www.xiaonuo.vip]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Snowy采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Snowy源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/xiaonuobase/snowy
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/xiaonuobase/snowy
6.若您的项目无法满足以上几点，可申请商业授权，获取Snowy商业授权许可，请在官网购买授权，地址为 https://www.xiaonuo.vip
 */
package vip.xiaonuo.modular.suppdata.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vip.xiaonuo.core.exception.ServiceException;
import vip.xiaonuo.core.factory.PageFactory;
import vip.xiaonuo.core.pojo.page.PageResult;
import vip.xiaonuo.core.util.PoiUtil;
import vip.xiaonuo.modular.suppdata.Result.SuppDataResult;
import vip.xiaonuo.modular.suppdata.entity.SuppData;
import vip.xiaonuo.modular.suppdata.enums.SuppDataExceptionEnum;
import vip.xiaonuo.modular.suppdata.mapper.SuppDataMapper;
import vip.xiaonuo.modular.suppdata.param.SuppDataParam;
import vip.xiaonuo.modular.suppdata.service.SuppDataService;
import vip.xiaonuo.modular.suppperson.entity.SuppPerson;
import vip.xiaonuo.modular.suppperson.service.SuppPersonService;
import vip.xiaonuo.modular.suppsort.service.SuppSortService;
import vip.xiaonuo.modular.task.enums.TaskExceptionEnum;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *  供应商资料service接口实现类
 *
 * @author zjk
 * @date 2022-08-04 09:12:05
 */
@Service
public class SuppDataServiceImpl extends ServiceImpl<SuppDataMapper, SuppData> implements SuppDataService {
    @Resource
    SuppSortService suppSortService;
    @Resource
    SuppPersonService suppPersonService;

    @Override
    public PageResult<SuppDataResult> page(SuppDataParam suppDataParam) {
        final String sqlTableName = "n.supp_sort_id";
        QueryWrapper<SuppDataResult> queryWrapper = new QueryWrapper<>();
        if (ObjectUtil.isNotNull(suppDataParam)) {

            // 根据供应商资料名称 查询
            if (ObjectUtil.isNotEmpty(suppDataParam.getSuppDataName())) {
                queryWrapper.lambda().like(SuppData::getSuppDataName,suppDataParam.getSuppDataName());
            }
            // 根据社会统一信用代码 查询
            if (ObjectUtil.isNotEmpty(suppDataParam.getCode())) {
                queryWrapper.lambda().like(SuppData::getCode, suppDataParam.getCode());
            }
            // 根据备注 查询
            if (ObjectUtil.isNotEmpty(suppDataParam.getRemark())) {
                queryWrapper.like("n.remark", suppDataParam.getRemark());
            }
            // 根据供应商类型 查询
            if (ObjectUtil.isNotEmpty(suppDataParam.getSuppSortId())) {
                List<Long> childIdList = suppSortService.getChildIdListById(suppDataParam.getSuppSortId());
                if (!childIdList.isEmpty()) {
                    queryWrapper.nested(item -> item.eq(sqlTableName, suppDataParam.getSuppSortId()).or().in(sqlTableName, childIdList));
                } else {
                    queryWrapper.eq(sqlTableName, suppDataParam.getSuppSortId());
                }
            }
            }
        queryWrapper.orderByDesc("n.create_time");
        return new PageResult<>(baseMapper.page(PageFactory.defaultPage(), queryWrapper));
    }

    @Override
    public List<SuppData> list(SuppDataParam suppDataParam) {
        return this.list();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SuppDataParam suppDataParam) {
        /*
         *  ================================= 供应商资料数据处理 start =================================
         */
        //校验参数，检查是否存在相同的名称
        checkParam(suppDataParam,false);
        //社会统一信用代码校验
//      checkUscc(suppDataParam.getCode());
        SuppData suppData = new SuppData();
        BeanUtil.copyProperties(suppDataParam, suppData);
        this.save(suppData);
        /*
         *  ================================= 供应商资料数据处理 end =================================
         */
        /*
         *  ================================= 联系人数据处理 start =================================
         */
        if(ObjectUtil.isNotEmpty(suppDataParam.getSuppPersonList())){
            List<SuppPerson> suppPersonList = suppDataParam.getSuppPersonList();
            suppPersonList.forEach(suppPerson ->{
                //设置供应商资料id
                if(ObjectUtil.isEmpty(suppPerson.getSuppDataId())){
                    suppPerson.setSuppDataId(suppData.getId());
                }
                //校验供应商资料名称和联系人不能为空
                if(ObjectUtil.isEmpty(suppPerson.getSuppDataId())){
                    throw new ServiceException(SuppDataExceptionEnum.NOT_NULL_SUPP_DATA_ID);
                }
                if(ObjectUtil.isEmpty(suppPerson.getSuppPerson())){
                    throw new ServiceException(SuppDataExceptionEnum.NOT_NULL_SUPP_PERSON);
                }
            });
            suppPersonService.saveBatch(suppPersonList);

        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(List<SuppDataParam> suppDataParamList) {
        //查询所有联系人
        List<SuppPerson>suppPeople=suppPersonService.list();
        //将联系人按供应商资料分组
        Map<Long,List<SuppPerson>>suppPersonGroupByWorkOrderId=suppPeople.stream()
                .collect(Collectors.groupingBy(SuppPerson::getSuppDataId));
        if (ObjectUtil.isEmpty(suppDataParamList)){
            return;
        }
        List<Long>suppDataIds=new ArrayList<>();
        List<Long>personIds=new ArrayList<>();
        suppDataParamList.forEach(suppDataParam -> {
            suppDataIds.add(suppDataParam.getId());
            List<SuppPerson>suppPersonList=suppPersonGroupByWorkOrderId.get(suppDataParam.getId());
            if (ObjectUtil.isNotEmpty(suppPersonList)){
                suppPersonList.forEach(suppPerson->{
                    personIds.add(suppPerson.getId());
                });
            }
        });
        this.removeByIds(suppDataIds);
        suppPersonService.removeByIds(personIds);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void edit(SuppDataParam suppDataParam) {
        /*
         *  ================================= 供应商资料数据处理 start =================================
         */
        //校验参数，检查是否存在相同的名称
        checkParam(suppDataParam, true);
//        //社会统一信用代码校验
//        checkUscc(suppDataParam.getCode());
        SuppData suppData = this.querySuppData(suppDataParam);
        BeanUtil.copyProperties(suppDataParam, suppData);
        this.updateById(suppData);
        /*
         *  ================================= 供应商资料数据处理 end =================================
         */
        /*
         *  ================================= 联系人数据处理 start =================================
         */

        // 联系人map数据准备（防止循环调用sql）
        Map<Long,SuppPerson>dbSuppPersonMap= new HashMap<>();
        suppPersonService.list().forEach(item->{
            dbSuppPersonMap.put(item.getId(), item);
        });

        //定义新增,编辑容器
        List<SuppPerson> addPersonList = new ArrayList<>();
        List<SuppPerson> editPersonList = new ArrayList<>();
        List<Long> editPersonIdList = new ArrayList<>();

        // 无id则为新增，有id则为编辑
        List<SuppPerson> paramPersonList =suppDataParam.getSuppPersonList();
        if(ObjectUtil.isNotEmpty(paramPersonList)){
            paramPersonList.forEach(item->{
                if(ObjectUtil.isEmpty(item.getId())){
                    addPersonList.add(item);
                } else {
                    editPersonList.add(item);
                    editPersonIdList.add(item.getId());
                }
            });
        }

        // 数据库中不在编辑id容器中，就是要被删除
        QueryWrapper<SuppPerson> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(SuppPerson::getSuppDataId,suppData.getId());
        if(ObjectUtil.isNotEmpty(editPersonList)){
            queryWrapper.lambda().notIn(SuppPerson::getId, editPersonIdList);
        }
        List<SuppPerson> delPersonList = suppPersonService.list(queryWrapper);

        // 批量新增以及数据初始化
        if(ObjectUtil.isNotEmpty(addPersonList)){
            //将联系人list遍历设置供应商资料id
            addPersonList.forEach(item->{
                item.setSuppDataId(suppData.getId());
                suppPersonService.save(item);
            });
        }

        // 批量更新联系人数据（防止有字段支持空更新)
        if (ObjectUtil.isNotEmpty(editPersonList)){
            List<SuppPerson>dbEditPersonList = new ArrayList<>();
            editPersonList.forEach(item->{
                // 设置供应商资料id
                item.setSuppDataId(suppData.getId());
                // 防止有字段支持空更新
                if (ObjectUtil.isNotEmpty(suppData.getId())){
                    SuppPerson dbPerson = dbSuppPersonMap.get(item.getId());
                    if (ObjectUtil.isNull(dbPerson)) {
                        throw new ServiceException(TaskExceptionEnum.NOT_EXIST);
                    }
                    BeanUtil.copyProperties(item, dbPerson);
                    dbEditPersonList.add(dbPerson);
                }
            });
            suppPersonService.updateBatchById(dbEditPersonList);
        }

        // 执行联系人的批量删除以及关联数据
        if(ObjectUtil.isNotEmpty(delPersonList)){
            List<Long> delPersonIdList = new ArrayList<>();
            delPersonList.forEach(item->{
                delPersonIdList.add(item.getId());
            });
            // 批量删除联系人
            suppPersonService.removeByIds(delPersonIdList);
        }
        /*
         *  ================================= 联系人数据处理 end =================================
         */

    }

    @Override
    public SuppData detail(SuppDataParam suppDataParam) {
        return this.querySuppData(suppDataParam);
    }

    /**
     * 获取 供应商资料
     *
     * @author zjk
     * @date 2022-08-04 09:12:05
     */
    private SuppData querySuppData(SuppDataParam suppDataParam) {
        SuppData suppData = this.getById(suppDataParam.getId());
        if (ObjectUtil.isNull(suppData)) {
            throw new ServiceException(SuppDataExceptionEnum.NOT_EXIST);
        }
        return suppData;
    }

    @Override
    public void export(SuppDataParam suppDataParam) {
        List<SuppData> list = this.list(suppDataParam);
        PoiUtil.exportExcelWithStream("SnowySuppData.xls", SuppData.class, list);
    }

    /**
     * 校验参数，检查是否存在相同的名称
     */
    private void checkParam(SuppDataParam suppDataParam, boolean isExcludeSelf) {
        Long id = suppDataParam.getId();
        String name = suppDataParam.getSuppDataName();

        LambdaQueryWrapper<SuppData>queryWrapperByName=new LambdaQueryWrapper<>();
        queryWrapperByName.eq(SuppData::getSuppDataName, name);

        if (isExcludeSelf) {
            queryWrapperByName.ne(SuppData::getId, id);
        }
        int countByName = this.count(queryWrapperByName);

        if (countByName >= 1) {
            throw new ServiceException(1, "供应商名称重复，请检查");
        }

        String code= suppDataParam.getCode();
        LambdaQueryWrapper<SuppData> queryWrapperByCode = new LambdaQueryWrapper<>();
        queryWrapperByCode.eq(SuppData::getCode, code);
        int countByCode=this.count(queryWrapperByCode);
        if(countByCode >=1){
            throw new ServiceException(2, "社会统一信用编码重复，请检查编码参数");
        }
    }


    }
//    /**
//     * 校验参数，检查社会统一信用代码
//     */
//    public static boolean checkUscc(String testUscc)
//    {
//        if(testUscc.length()!=18)
//        {
//            throw new ServiceException(1,"统一社会信用代码长度错误");
//        }
//        //用于存放权值
//        int[] weight = {1,3,9,27,19,26,16,17,20,29,25,13,8,24,10,30,28};
//        //用于计算当前判断的统一社会信用代码位数
//        int index;
//        //用于存放当前位的统一社会信用代码
//        char testc;
//        //用于存放代码字符和加权因子乘积之和
//        int tempSum=0;
//        int tempNum;
//        for(index = 0;index<= 16 ;index++)
//        {
//            testc=testUscc.charAt(index);
//
//            if(index==0)
//            {
//                if(testc!='1'&&testc!='5'&&testc!='9'&&testc!='Y')
//                {
//                    throw new ServiceException(2,"统一社会信用代码中登记管理部门代码错误");
//                }
//            }
//
//            if(index==1)
//            {
//                if(testc!='1'&&testc!='2'&&testc!='3'&&testc!='9')
//                {
//                    throw new ServiceException(3,"统一社会信用代码中机构类别代码错误");
//                }
//            }
//
//            tempNum=charToNum(testc);
//            //验证代码中是否有错误字符
//            if(tempNum!=-1)
//            {
//                tempSum+=weight[index]*tempNum;
//            }
//            else
//            {
//                throw new ServiceException(4,"统一社会信用代码中出现错误字符");
//            }
//        }
//        tempNum=31-tempSum%31;
//        if(tempNum== 31){  tempNum=0;}
//        //按照GB/T 17710标准对统一社会信用代码前17位计算校验码，并与第18位校验位进行比对
//        if (charToNum(testUscc.charAt(17))==tempNum){
//            return true;
//        }
//        else{
//            return false;
//        }
//
//    }
//
//    public static int charToNum(char c)
//    {
//        switch (c)
//        {
//            case '0':
//                return 0;
//            case '1':
//                return 1;
//            case '2':
//                return 2;
//            case '3':
//                return 3;
//            case '4':
//                return 4;
//            case '5':
//                return 5;
//            case '6':
//                return 6;
//            case '7':
//                return 7;
//            case '8':
//                return 8;
//            case '9':
//                return 9;
//            case 'A':
//                return 10;
//            case 'B':
//                return 11;
//            case 'C':
//                return 12;
//            case 'D':
//                return 13;
//            case 'E':
//                return 14;
//            case 'F':
//                return 15;
//            case 'G':
//                return 16;
//            case 'H':
//                return 17;
//            case 'J':
//                return 18;
//            case 'K':
//                return 19;
//            case 'L':
//                return 20;
//            case 'M':
//                return 21;
//            case 'N':
//                return 22;
//            case 'P':
//                return 23;
//            case 'Q':
//                return 24;
//            case 'R':
//                return 25;
//            case 'T':
//                return 26;
//            case 'U':
//                return 27;
//            case 'W':
//                return 28;
//            case 'X':
//                return 29;
//            case 'Y':
//                return 30;
//            default:
//                return -1;
//        }
//    }
