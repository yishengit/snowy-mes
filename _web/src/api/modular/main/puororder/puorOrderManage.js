import { axios } from '@/utils/request'

/**
 * 查询采购订单
 *
 * @author jiaxin
 * @date 2022-07-27 16:13:02
 */
export function puorOrderPage (parameter) {
  return axios({
    url: '/puorOrder/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 采购订单列表
 *
 * @author jiaxin
 * @date 2022-07-27 16:13:02
 */
export function puorOrderList (parameter) {
  return axios({
    url: '/puorOrder/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加采购订单
 *
 * @author jiaxin
 * @date 2022-07-27 16:13:02
 */
export function puorOrderAdd (parameter) {
  return axios({
    url: '/puorOrder/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑采购订单
 *
 * @author jiaxin
 * @date 2022-07-27 16:13:02
 */
export function puorOrderEdit (parameter) {
  return axios({
    url: '/puorOrder/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除采购订单
 *
 * @author jiaxin
 * @date 2022-07-27 16:13:02
 */
export function puorOrderDelete (parameter) {
  return axios({
    url: '/puorOrder/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出采购订单
 *
 * @author jiaxin
 * @date 2022-07-27 16:13:02
 */
export function puorOrderExport (parameter) {
  return axios({
    url: '/puorOrder/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
