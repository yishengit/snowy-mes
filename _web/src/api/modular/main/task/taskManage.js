import { axios } from '@/utils/request'

/**
 * 查询任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskPage (parameter) {
  return axios({
    url: '/task/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 任务列表
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskList (parameter) {
  return axios({
    url: '/task/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskAdd (parameter) {
  return axios({
    url: '/task/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskEdit (parameter) {
  return axios({
    url: '/task/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskDelete (parameter) {
  return axios({
    url: '/task/delete',
    method: 'post',
    data: parameter
  })
}
/**
 * 开启任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskStart (parameter) {
  return axios({
    url: '/task/start',
    method: 'post',
    data: parameter
  })
}
/**
 * 开启任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskEnd (parameter) {
  return axios({
    url: '/task/end',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出任务
 *
 * @author bwl
 * @date 2022-06-02 09:27:11
 */
export function taskExport (parameter) {
  return axios({
    url: '/task/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
