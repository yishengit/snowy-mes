import { axios } from '@/utils/request'

/**
 * 查询采购细明
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
 */
export function puorDetailPage (parameter) {
  return axios({
    url: '/puorDetail/page',
    method: 'get',
    params: parameter
  })
}

/**
 * 采购细明列表
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
 */
export function puorDetailList (parameter) {
  return axios({
    url: '/puorDetail/list',
    method: 'get',
    params: parameter
  })
}

/**
 * 添加采购细明
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
 */
export function puorDetailAdd (parameter) {
  return axios({
    url: '/puorDetail/add',
    method: 'post',
    data: parameter
  })
}

/**
 * 编辑采购细明
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
 */
export function puorDetailEdit (parameter) {
  return axios({
    url: '/puorDetail/edit',
    method: 'post',
    data: parameter
  })
}

/**
 * 删除采购细明
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
 */
export function puorDetailDelete (parameter) {
  return axios({
    url: '/puorDetail/delete',
    method: 'post',
    data: parameter
  })
}

/**
 * 导出采购细明
 *
 * @author jiaxin
 * @date 2022-07-27 15:57:25
 */
export function puorDetailExport (parameter) {
  return axios({
    url: '/puorDetail/export',
    method: 'get',
    params: parameter,
    responseType: 'blob'
  })
}
